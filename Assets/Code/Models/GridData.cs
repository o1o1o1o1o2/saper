using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Code.Models
{
    public class GridData<T> 
    {
        private Vector2Int _gridSize;
        
        private T[,] _gridCells;
        
        private GridEnum[,] _gridFlags;

        private Dictionary<int,bool> _emptyCells;
    
        public GridData(Vector2Int gridSize)
        {
            _gridSize = gridSize;
            _gridCells = new T[_gridSize.y,_gridSize.x];
            _gridFlags = new GridEnum[_gridSize.y,_gridSize.x];
            _emptyCells = new Dictionary<int,bool>(_gridSize.y*_gridSize.x);
            for (int i = 0; i < _emptyCells.Count; i++) _emptyCells.Add(i,true);
        }

        public void GridRandomFill(int contentNum, T item)
        {
            for (int i = 0; i < contentNum - 1; i++)
            {
                int index = Random.Range(0, _emptyCells.Count - 1);
                _emptyCells.Remove(index);
                Vector2Int pos = Get2DPositionFromIndex(index);
                _gridCells[pos.y,pos.x] = item;
            }
        }
        
        public T GetValue(Vector2Int pos)
        {
            return _gridCells[pos.y, pos.x];
        }

        public void SetValue(Vector2Int pos,T value)
        {
            _gridCells[pos.y, pos.x] = value;
        }
        
        public void AddFlags(Vector2Int pos,GridEnum value)
        {
            _gridFlags[pos.y, pos.x] |= value;
        }
        
        public void RemoveFlags(Vector2Int pos,GridEnum value)
        {
            _gridFlags[pos.y, pos.x] &= ~value;
        }
        
        public GridEnum GetFlags(Vector2Int pos)
        {
            return _gridFlags[pos.y, pos.x];
        }

        public void GridFill(T item)
        {
            for (int y = 0; y < _gridCells.GetLength(0); y++)
            {
                for (int x = 0; x < _gridCells.GetLength(1); x++)
                {
                    _gridCells[y,x] = item;
                    _gridFlags[y, x] = 0;
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Vector2Int Get2DPositionFromIndex(int index)
        {
            return new Vector2Int(index % _gridSize.x,index / (_gridSize.x * _gridSize.y));
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int GetIndexFrom2DPosition(Vector2Int position)
        {
            return position.y * _gridSize.x + position.x;
        }

    }
}

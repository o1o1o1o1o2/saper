using System;
using Code.Core.ScriptableObjects;
using UnityEngine;

public class GameBaseModel
{
    private GameSettingsSo _gameSettingsSo;
    public event Action<int> shovelNumChanged;
    public event Action gameStartedEvent;
    public event Action<bool> gameEndedEvent;
    public event Action<int> goldNumChanged;
    public int notCollectedGold;
    public int shovelsNum;
    public int goldNum;

    public GameBaseModel(GameSettingsSo gameSettingsSo)
    {
        _gameSettingsSo = gameSettingsSo;
    }

    public void AddGold()
    {
        goldNum++;
        goldNumChanged?.Invoke(goldNum);
        if (_gameSettingsSo.goldToWin == goldNum) gameEndedEvent?.Invoke(true);
    }

    public void StartGame()
    {
        gameStartedEvent?.Invoke();
        shovelsNum = _gameSettingsSo.shovelsNum;
        goldNum = 0;
        notCollectedGold = 0;
        goldNumChanged?.Invoke(goldNum);
        shovelNumChanged?.Invoke(shovelsNum);
    }

    public void AddNotCollectedGold() => notCollectedGold++;

    public void SubtractShovel()
    {
        shovelsNum--;
        if (_gameSettingsSo.goldToWin - (goldNum + notCollectedGold + shovelsNum) > 0) gameEndedEvent?.Invoke(false);
        shovelNumChanged?.Invoke(shovelsNum);
    }

}

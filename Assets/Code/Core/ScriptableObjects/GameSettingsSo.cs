using System;
using UnityEngine;

namespace Code.Core.ScriptableObjects
{
    [Serializable]
    [CreateAssetMenu(fileName = "GameSettings", menuName = "SO/Settings/GameSettings", order = 1)]
    public class GameSettingsSo : ScriptableObject
    {
        public GameObject cellPrefab;
        public Vector2Int gridSize;
        public Vector2Int cellSize;
        public int gridDepth;
        public int shovelsNum;
        public int goldToWin;
        public float goldChance;
        public Vector2 originOffset;
    }
}

using System;
using UnityEngine;
using Zenject;

public class GameStatesMachine : MonoBehaviour
{
    private StateMachine _stateMachine;
    
    private StartScreenPresenter _startScreenState;
    private MainScreenPresenter _mainScreenState;

    [Inject]
    public void Construct(StartScreenPresenter startScreenPresenter, MainScreenPresenter mainScreenPresenter)
    {
        _startScreenState = startScreenPresenter;
        _mainScreenState = mainScreenPresenter;
    }
    
    private void Awake()
    {
        _stateMachine = new StateMachine();
        
        AddTransition(_startScreenState, _mainScreenState, () => _startScreenState.startGame);
        AddTransition(_mainScreenState, _startScreenState, () => _mainScreenState.gameEnded);

        void AddTransition(IState from, IState to, Func<bool> condition) => _stateMachine.AddTransition(from, to, condition);
       
       _stateMachine.SetState(_startScreenState);
    }

    void Update() =>_stateMachine.Tick();
    
}

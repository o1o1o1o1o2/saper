using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class StartScreenView : ScreenView
{
    [SerializeField] private TextMeshProUGUI gameEndedText;
    
    [SerializeField] private Button startButton;

    public void HideGameEndedText() => gameEndedText.enabled = false;
    public void SetGameEndedText(string text)
    {
        gameEndedText.enabled = true;
        gameEndedText.text = text;
    }

    public void StartButtonAddListner(UnityAction method) => startButton.onClick.AddListener(method);
    public void StartButtonRemoveListner(UnityAction method) => startButton.onClick.RemoveListener(method);
}

using System;
using TMPro;
using UnityEngine;

namespace Code.UI.Views
{
    public class MainScreenView : ScreenView
    {
        [SerializeField]
        private TextMeshProUGUI goldNumText;
        [SerializeField]
        private TextMeshProUGUI shovelsNumText;

        private string s = "{0}";

        public void Init(GameBaseModel gameBaseModel)
        {
            goldNumText.SetText(s, gameBaseModel.goldNum);
            shovelsNumText.SetText(s, gameBaseModel.shovelsNum);
        }

        public void SetGoldNum(int num) => goldNumText.SetText(s, num);
        public void SetShovelsNum(int num) => shovelsNumText.SetText(s, num);

    }
}

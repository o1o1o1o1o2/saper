﻿using System;
using UnityEngine;

public class ScreenView : MonoBehaviour
{
    private Canvas _canvas;

    protected virtual void Awake()
    {
        _canvas = GetComponent<Canvas>();
    }

    public void Enable() => _canvas.enabled = true;
    
    public void Disable() => _canvas.enabled = false;
}

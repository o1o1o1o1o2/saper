public class StartScreenPresenter : IState, IStateEnter,IStateExit
{
    public IStateEnter IStateEnter { get => this; }
    public IStateTick IStateTick { get => null; }
    public IStateExit IStateExit { get => this; }

    private GameBaseModel _gameBaseModel;
    private StartScreenView _startScreenView;

    public bool startGame;
    
    public StartScreenPresenter(GameBaseModel gameBaseModel, StartScreenView startScreenView)
    {
        _gameBaseModel = gameBaseModel;
        _startScreenView = startScreenView;
        _startScreenView.HideGameEndedText();
        _gameBaseModel.gameEndedEvent += GameEnded;
    }

    private void StartGame() => startGame = true;

    private void GameEnded(bool result)
    {
        if (result) _startScreenView.SetGameEndedText("You Win");
        else  _startScreenView.SetGameEndedText("You Loose");
    }

    public void OnEnter()
    {
        _startScreenView.Enable();
        _startScreenView.StartButtonAddListner(StartGame);
    }

    public void OnExit()
    {
        _startScreenView.Disable();
        _startScreenView.StartButtonRemoveListner(StartGame);
        startGame = false;
    }
}

using Code.Components;
using Code.UI.Views;
using UnityEngine;

public class MainScreenPresenter: IState, IStateEnter, IStateExit
{
    public IStateEnter IStateEnter { get => this; }
    public IStateTick IStateTick { get => null; }
    public IStateExit IStateExit { get => this; }
    
    private GameBaseModel _gameBaseModel;
    private MainScreenView _mainScreenView;

    public bool gameEnded;

    public MainScreenPresenter(GameBaseModel gameBaseModel, MainScreenView mainScreenView)
    {
        _gameBaseModel = gameBaseModel;
        _mainScreenView = mainScreenView;
    }

    private void GameEnded(bool result) => gameEnded = true;

    private void ChangeGoldNum(int num) => _mainScreenView.SetGoldNum(num);
    private void ChangeShovelNum(int num) => _mainScreenView.SetShovelsNum(num);

    public void OnEnter()
    {
        _gameBaseModel.StartGame();
        foreach (var gold in GameObject.FindGameObjectsWithTag("Gold")) GameObject.DestroyImmediate(gold); //todo dirty
        _gameBaseModel.goldNumChanged += ChangeGoldNum;
        _gameBaseModel.shovelNumChanged += ChangeShovelNum;
        _gameBaseModel.gameEndedEvent += GameEnded;
        _mainScreenView.Init(_gameBaseModel);
        _mainScreenView.Enable();
    }

    public void OnExit()
    {
        _mainScreenView.Disable();
        _gameBaseModel.goldNumChanged -= ChangeGoldNum;
        _gameBaseModel.shovelNumChanged -= ChangeShovelNum;
        _gameBaseModel.gameEndedEvent -= GameEnded;
        gameEnded = false;
    }
}

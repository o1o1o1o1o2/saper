using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class ChestControl : MonoBehaviour, IDropHandler
{
    private GameBaseModel _gameBaseModel;
    
    [Inject]
    public void Construct(GameBaseModel gameBaseModel)
    {
        _gameBaseModel = gameBaseModel;
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            _gameBaseModel.AddGold();
        }
    }
}

using Code.Core.ScriptableObjects;
using Code.Models;
using UnityEngine;
using Zenject;

namespace Code.Components
{
    public class GridController : MonoBehaviour
    {
        private Camera _camera;
        private GameSettingsSo _gameSettings;
        private GridData<ushort> _gridData;

        private GameObject _cellPrefab;
        private Vector2 _cellSize;
        private Vector2 _originOffset;
        private Vector2 _gridSize;
        public Vector2 gridSize => _gridSize;

        private SpriteRenderer[,] _visualCells;

        private GameBaseModel _gameBaseModel;

        [Inject] private Gold.GoldFactory _goldFactory;

        [Inject]
        public void Construct(GameSettingsSo gameSettings,GameBaseModel gameBaseModel, GridData<ushort> gridData)
        {
            _gameSettings = gameSettings;
            _gameBaseModel = gameBaseModel;
            _gridData = gridData;

            _cellSize = gameSettings.cellSize;
            _originOffset = gameSettings.originOffset;
            _cellPrefab = gameSettings.cellPrefab;
            _cellPrefab.transform.localScale = _cellSize;
   
            _visualCells = new SpriteRenderer[_gameSettings.gridSize.y,_gameSettings.gridSize.x];
            
            _gridSize = new Vector2(_gameSettings.gridSize.x * _gameSettings.cellSize.x,_gameSettings.gridSize.y * _gameSettings.cellSize.y);
            _camera = Camera.main;
            
            for (int y = 0; y < _gameSettings.gridSize.y; y++)
            {
                for (int x = 0; x < _gameSettings.gridSize.x; x++)
                {
                    _visualCells[y, x] = Instantiate(_cellPrefab, new Vector2(x * _cellSize.x, y * _cellSize.y) + _cellSize * 0.5f + _originOffset,Quaternion.identity, gameObject.transform).GetComponent<SpriteRenderer>();
                }
            }

            _gameBaseModel.gameStartedEvent += Restart;
        }

        public void Restart()
        {
            _gridData.GridFill((ushort)_gameSettings.gridDepth);
            for (int y = 0; y < _gameSettings.gridSize.y; y++)
            {
                for (int x = 0; x < _gameSettings.gridSize.x; x++)
                {
                    _visualCells[y, x].color = Color.white;
                }
            }
        }

        public void Dig(Vector2 worldPos)
        {
            if (IsValidPosition(worldPos))
            {
                var arrayPos = GetArrayPosFromWorldPos(worldPos);
                
                if ((_gridData.GetFlags(arrayPos) & GridEnum.Blocked) != 0) return;
                
                var value = _gridData.GetValue(arrayPos);
                if (value > 0 && _gameBaseModel.shovelsNum > 0)
                {
                    value--;
                    _gridData.SetValue(arrayPos,value);
                    _visualCells[arrayPos.y, arrayPos.x].color -= new Color(0.333f,0.333f,0.333f,0);
                    if (SpawnGold(arrayPos)) BlockCell(arrayPos);
                    _gameBaseModel.SubtractShovel();
                }
            }
        }

        public void BlockCell(Vector2Int arrayPos) => _gridData.AddFlags(arrayPos,GridEnum.Blocked);
        
        public void UnblockCell(Vector2Int arrayPos) => _gridData.RemoveFlags(arrayPos,GridEnum.Blocked);

        public bool SpawnGold(Vector2Int arrayPos)
        {
            if (Random.Range(0.0f, 1.0f) <= _gameSettings.goldChance)
            {
                var g =_goldFactory.Create();
                g.Setup(arrayPos,GetScreenPosFromArrayPos(arrayPos));
                _gameBaseModel.AddNotCollectedGold();
                return true;
            }

            return false;
        }

        private Vector2Int GetArrayPosFromWorldPos(Vector3 worldPos)
        {
            return new Vector2Int(Mathf.FloorToInt((worldPos.x - _originOffset.x) / _cellSize.x), Mathf.FloorToInt((worldPos.y - _originOffset.y) / _cellSize.y));
        }
        
        private Vector3 GetScreenPosFromArrayPos(Vector2Int arrayPos)
        {
            var pos = new Vector2(arrayPos.x * _cellSize.x + _originOffset.x, arrayPos.y * _cellSize.y + _originOffset.y) + _cellSize * 0.5f;
            return _camera.WorldToScreenPoint(pos);
        }
        
        private bool IsValidPosition(Vector2 worldPos)
        {
            return (worldPos.x > _originOffset.x && worldPos.y > _originOffset.y && worldPos.x < _gridSize.x && worldPos.y < _gridSize.y);
        }
    }
}

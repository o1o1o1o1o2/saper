using Code.Components;
using Code.UI.Views;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class Gold : MonoBehaviour, IBeginDragHandler,IEndDragHandler,IDragHandler
{
    private GridController _gridController;
    private CanvasGroup _canvasGroup;
    private Canvas _canvas;
    private RectTransform _chestControlRT;
    private RectTransform _transform;
    
    private Vector3 _startPosition;
    private Vector2Int _arrayPos;

    [Inject]
    private void Construct(Canvas canvas, GridController gridController)
    {
        _canvas = canvas;
        _gridController = gridController;
        _transform = (RectTransform)transform;
        _transform.SetParent(canvas.GetComponentInChildren<MainScreenView>().transform);
        _canvasGroup = GetComponent<CanvasGroup>();
        _chestControlRT = (RectTransform)_canvas.GetComponentInChildren<ChestControl>().transform;
    }

    public void Setup(Vector2Int arrayPos, Vector3 screenPos)
    {
        _transform.position = screenPos;
        _startPosition = _transform.anchoredPosition;
        _arrayPos = arrayPos;
    }
    
    public void OnBeginDrag(PointerEventData eventData)
    {
        _canvasGroup.blocksRaycasts = false;
    }
    
    public void OnDrag(PointerEventData eventData)
    {
        _transform.anchoredPosition += eventData.delta / _canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!RectTransformUtility.RectangleContainsScreenPoint(_chestControlRT, eventData.position)) _transform.anchoredPosition = _startPosition;
        else
        {
            DestroyImmediate(gameObject);
            _gridController.UnblockCell(_arrayPos);
            return;
        }
        _canvasGroup.blocksRaycasts = true;
    }

    public class GoldFactory: PlaceholderFactory<Gold> { }
}

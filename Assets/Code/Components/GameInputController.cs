using Code.Components;
using Code.Shared.Input;
using UnityEngine;
using Zenject;

public class GameInputController: MonoBehaviour
{
    private Camera _mainCam;
    private GridController _gridController;
    private TouchInputManager _touchInputManager;
    
    [Inject]
    public void Construct(TouchInputManager touchInputManager, GridController gridController)
    {
        _gridController = gridController;
        _touchInputManager = touchInputManager;
    }

    private void OnPointerDown(Touch touch)
    {
        _gridController.Dig(_mainCam.ScreenToWorldPoint(touch.position));
    }

    private void Start()
    {
        _mainCam = Camera.main;
    }

    private void OnEnable()
    {
        _touchInputManager.TouchBeginEvent += OnPointerDown;
    }
    
    private void OnDisable()
    {
        _touchInputManager.TouchBeginEvent -= OnPointerDown;
    }
}

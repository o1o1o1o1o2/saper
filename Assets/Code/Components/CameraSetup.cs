using Code.Components;
using UnityEngine;
using Zenject;

public enum MaskingType
{
    Pixels,
    Percents
}

public class CameraSetup : MonoBehaviour
{
    [SerializeField] private Vector3 cameraOffset;
    [SerializeField] private MaskingType maskingType;

    [SerializeField, SerializeProperty("horizontalOffset")]
    private int _horizontalOffset = 10;

    public int horizontalOffset
    {
        get => _horizontalOffset;
        private set
        {
            if (value >= 0)
            {
                _horizontalOffset = value;
                SetupCamera();
            }
        }
    }

    [SerializeField, SerializeProperty("topOffset")]
    private int _topOffset = 10;

    public int topOffset
    {
        get => _topOffset;
        private set
        {
            if (value >= 0)
            {
                _topOffset = value;
                SetupCamera();
            }
        }
    }

    private Camera _camera;
    private Transform _cameraTransform;
    private GridController _gridController;

    [Inject]
    public void Construct(GridController gridController) => _gridController = gridController;

    public void Start()
    {
        _camera = Camera.main;
        if (_camera != null) _cameraTransform = _camera.transform;
        SetupCamera();
    }

    private void SetupCamera()
    {
        if (_camera == null) return;

        _cameraTransform.position = new Vector3(_gridController.gridSize.x / 2, _gridController.gridSize.y / 2, -10) + cameraOffset;

        float workingSizeInPixels;

        float cameraMinSize;
        float offset;
        float gridSizeF;
        
        if (_camera.aspect < 1) cameraMinSize = _camera.pixelWidth;
        
        else cameraMinSize = _camera.pixelHeight;
        
        if (_gridController.gridSize.x > _gridController.gridSize.y)
        {
            offset = _horizontalOffset;
            gridSizeF = _gridController.gridSize.x / 2 / _camera.aspect;
        }
        else
        {
            offset = _topOffset;
            gridSizeF = _gridController.gridSize.y / 2;
        }

        if (maskingType == MaskingType.Percents) workingSizeInPixels = cameraMinSize - cameraMinSize / 100 * offset * 2;
        else workingSizeInPixels = cameraMinSize - offset * 2;
        
        if (workingSizeInPixels > 0) _camera.orthographicSize = cameraMinSize / (workingSizeInPixels / gridSizeF) ;
    }
}

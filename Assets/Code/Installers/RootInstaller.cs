using Code.Components;
using Code.Core.ScriptableObjects;
using Code.Models;
using Code.Shared.Input;
using Code.UI.Views;
using UnityEngine;
using Zenject;

namespace Code.Installers
{
    [CreateAssetMenu(fileName = "RootInstaller", menuName = "SO/Installers", order = 1)]
    public class RootInstaller : ScriptableObjectInstaller<RootInstaller>
    {
        [SerializeField] private GameStatesMachine gameStatesMachine;
        [SerializeField] private Canvas canvas;
        [SerializeField] private TouchInputManager touchInputManager;
        [SerializeField] private GridController gridController;
        [SerializeField] private GameSettingsSo gameSettings;
        [SerializeField] private Gold gold;

        public override void InstallBindings()
        {
            Container.Bind<TouchInputManager>().FromComponentInNewPrefab(touchInputManager).AsSingle().NonLazy();
            
            Container.Bind<GameSettingsSo>().FromScriptableObject(gameSettings).AsSingle().NonLazy();
            Container.Bind<GameBaseModel>().FromNew().AsSingle().NonLazy();

            Container.Bind<GridData<ushort>>().AsSingle().WithArguments(gameSettings.gridSize).NonLazy();
            Container.Bind<GridController>().FromComponentInNewPrefab(gridController).AsSingle().NonLazy();

            Container.BindFactory<Gold, Gold.GoldFactory>().FromComponentInNewPrefab(gold);
            
            Container.Bind<Canvas>().FromComponentInNewPrefab(canvas).AsSingle().NonLazy();
            
            Container.Bind<StartScreenView>().FromComponentInHierarchy(canvas).AsSingle();
            Container.Bind<MainScreenView>().FromComponentInHierarchy(canvas).AsSingle();
            
            Container.BindInterfacesAndSelfTo<StartScreenPresenter>().FromNew().AsSingle().NonLazy();
            Container.BindInterfacesAndSelfTo<MainScreenPresenter>().FromNew().AsSingle().NonLazy();
            
            Container.BindInterfacesTo<ChestControl>().FromComponentInHierarchy().AsSingle().NonLazy();
            Container.Bind<GameStatesMachine>().FromComponentInNewPrefab(gameStatesMachine).AsSingle().NonLazy();
        }
    }
}
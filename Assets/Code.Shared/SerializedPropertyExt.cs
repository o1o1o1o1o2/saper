using System;
using System.ComponentModel;
using System.Reflection;
using UnityEditor;
using UnityEngine;

public static class SerializedPropertyExt
{
#if UNITY_EDITOR
    public static object GetParentObject(this SerializedProperty property, string path)
    {
        var fields = path.Split('.');
        object obj = property.serializedObject.targetObject;

        if (fields.Length == 1)
            return obj;

        FieldInfo info = obj.GetType().GetField(fields[0], BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        obj = info.GetValue(obj);
        
        return property.GetParentObject(string.Join(".", fields, 1, fields.Length - 1));
    }
    
    private static MethodInfo _sIntFieldMethod = null;
    private static object _sRecycledEditor = null;
    
    public static int MyIntFieldInternal(Rect displayRect, Rect dragHotZone, int value, GUIStyle style)
    {
        if (_sIntFieldMethod == null || _sRecycledEditor == null)
        {
            Type editorGUIType = typeof(EditorGUI);

            Type recycledTextEditorType = Assembly.GetAssembly(editorGUIType).GetType("UnityEditor.EditorGUI+RecycledTextEditor");
            Type[] argumentTypes = new Type[]
            {
                recycledTextEditorType,
                typeof(Rect),
                typeof(Rect),
                typeof(int),
                typeof(int),
                typeof(string),
                typeof(GUIStyle),
                typeof(bool),
                typeof(float)
            };
            _sIntFieldMethod = editorGUIType.GetMethod("DoIntField", BindingFlags.NonPublic | BindingFlags.Static, null, argumentTypes, null);

            FieldInfo fieldInfo = editorGUIType.GetField("s_RecycledEditor", BindingFlags.NonPublic | BindingFlags.Static);
            _sRecycledEditor = fieldInfo.GetValue(null);
        }

        int controlID = GUIUtility.GetControlID("EditorTextField".GetHashCode(), FocusType.Keyboard, displayRect);
        object[] fieldParams = new object[] {_sRecycledEditor, displayRect, dragHotZone, controlID, value, "#######0", style, true, 1f};

        return (int) _sIntFieldMethod.Invoke(null, fieldParams);
    }

    public static float MyFloatFieldInternal(Rect position, Rect dragHotZone, float value, [DefaultValue("EditorStyles.numberField")]
        GUIStyle style)
    {
        int controlID = GUIUtility.GetControlID("EditorTextField".GetHashCode(), FocusType.Keyboard, position);
        Type editorGUIType = typeof(EditorGUI);

        Type recycledTextEditorType = Assembly.GetAssembly(editorGUIType).GetType("UnityEditor.EditorGUI+RecycledTextEditor");
        Type[] argumentTypes = new Type[] {recycledTextEditorType, typeof(Rect), typeof(Rect), typeof(int), typeof(float), typeof(string), typeof(GUIStyle), typeof(bool)};
        MethodInfo doFloatFieldMethod = editorGUIType.GetMethod("DoFloatField", BindingFlags.NonPublic | BindingFlags.Static, null, argumentTypes, null);

        FieldInfo fieldInfo = editorGUIType.GetField("s_RecycledEditor", BindingFlags.NonPublic | BindingFlags.Static);
        object recycledEditor = fieldInfo.GetValue(null);

        object[] parameters = new object[] {recycledEditor, position, dragHotZone, controlID, value, "g7", style, true};

        return (float) doFloatFieldMethod.Invoke(null, parameters);
    }

#endif // UNITY_EDITOR
}
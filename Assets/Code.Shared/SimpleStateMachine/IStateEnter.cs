﻿

using System;

public interface IState
{
     IStateEnter IStateEnter { get; }
     IStateTick IStateTick { get; }
     IStateExit IStateExit { get; }
}

public interface IStateEnter
{
    void OnEnter();
}

public interface IStateTick
{
    void Tick();
}

public interface IStateExit
{
    void OnExit();
}

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public static class PointerExt
{
    private static readonly int _uiLayerNum = LayerMask.NameToLayer("UI");
    public static bool IsPointerOverUIElement(Vector3 position)
    {
        return IsPointerOverUIElement(GetEventSystemRaycastResults(position));
    }

    private static bool IsPointerOverUIElement(List<RaycastResult> eventSystemRaysastResults )
    {
        RaycastResult _curRaysastResult;
        for(int index = 0;  index < eventSystemRaysastResults.Count; index ++)
        {
            _curRaysastResult = eventSystemRaysastResults [index];
            if (_curRaysastResult.gameObject.layer == _uiLayerNum)
                return true;
        }
        return false;
    }
    
    private static List<RaycastResult> GetEventSystemRaycastResults(Vector3 position)
    {
        var eventData = new PointerEventData(EventSystem.current); 
        eventData.position =  position;
        List<RaycastResult> raysastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll( eventData, raysastResults );
        return raysastResults;
    }

}

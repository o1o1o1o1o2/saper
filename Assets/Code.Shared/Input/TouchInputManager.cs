using System;
using UnityEngine;
using Zenject;

namespace Code.Shared.Input
{
    [Serializable]
    public class TouchInputManager: MonoBehaviour
    {
        public delegate void TouchBeginDelegate(Touch touches);

        public event TouchBeginDelegate TouchBeginEvent;

        public delegate void TouchProcessDelegate(Touch touches);

        public event TouchProcessDelegate TouchProcessEvent;

        public delegate void TouchEndedDelegate(Touch touches);

        public event TouchEndedDelegate TouchEndedEvent;

        public delegate void TwoFingersBeginDelegate(Touch[] touches);

        public event TwoFingersBeginDelegate TwoFingersBeginEvent;

        public delegate void TwoFingersTouchProcessDelegate(Touch[] touches);

        public event TwoFingersTouchProcessDelegate TwoFingersTouchProcessEvent;

        public delegate void TwoFingersEndedDelegate(Touch[] touches);

        public event TwoFingersEndedDelegate TwoFingersEndedEvent;

        public delegate void ZoomDelegate(float y);

        public event ZoomDelegate ZoomEvent;
    
        public delegate void BackButtonDelegate();

        public event BackButtonDelegate BackButtonEvent;

        [SerializeField] private bool simulateTouch;
        private Touch[] _touches;
        private bool _twoFingersSimulated;
        private int _mouseButtonUpCounter;
        private Vector2 mousePrevPos;
        
        public void Start()
        {
            if (simulateTouch) _touches = new Touch[2];
        }
        
        public void Update()
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.Escape) || UnityEngine.Input.GetKeyDown(KeyCode.Return)) BackButtonEvent?.Invoke();

            if (simulateTouch)
            {
                if (UnityEngine.Input.mouseScrollDelta.magnitude > 0)
                {
                    ZoomEvent?.Invoke(UnityEngine.Input.mouseScrollDelta.y);
                }

                if (!UnityEngine.Input.GetMouseButtonDown(0) && !UnityEngine.Input.GetMouseButton(0) && !UnityEngine.Input.GetMouseButtonUp(0) || !ValidMousePosition(out Vector2 v)) return;
            
                if (UnityEngine.Input.GetKey(KeyCode.LeftAlt))
                {
                    if (!_twoFingersSimulated && UnityEngine.Input.GetMouseButtonDown(0))
                    {
                        _touches[0].position = v;
                        _twoFingersSimulated = true;
                    }
                    else if (_twoFingersSimulated)
                    {
                        if (UnityEngine.Input.GetMouseButtonUp(0)) _mouseButtonUpCounter++;

                        if (UnityEngine.Input.GetMouseButtonDown(0))
                        {
                            _touches[1].position = v;
                            TwoFingersBeginEvent?.Invoke(_touches);
                        }
                        else if (UnityEngine.Input.GetMouseButton(0))
                        {
                            _touches[1].position = v;
                            TwoFingersTouchProcessEvent?.Invoke(_touches);
                        }
                        else if (_mouseButtonUpCounter > 1 && UnityEngine.Input.GetMouseButtonUp(0))
                        {
                            TwoFingersEndedEvent?.Invoke(_touches);
                        }
                    }
                }
                else if (UnityEngine.Input.GetKeyUp(KeyCode.LeftAlt))
                {
                    _twoFingersSimulated = false;
                    TwoFingersEndedEvent?.Invoke(_touches);
                    _mouseButtonUpCounter = 0;
                }
                else
                {
                    if (UnityEngine.Input.GetMouseButtonDown(0))
                    {
                        mousePrevPos = v;
                        TouchBeginEvent?.Invoke(new Touch() {position = v});
                    }
                    else if (UnityEngine.Input.GetMouseButton(0))
                    {
                        TouchProcessEvent?.Invoke(new Touch() {position = v, deltaPosition = v - mousePrevPos});
                        mousePrevPos = v;
                    }
                    if (UnityEngine.Input.GetMouseButtonUp(0)) TouchEndedEvent?.Invoke(new Touch() {position = v});
                }
            }

            if (UnityEngine.Input.touchCount == 1)
            {
                var touch = UnityEngine.Input.GetTouch(0);
                if (PointerExt.IsPointerOverUIElement(touch.position)) return;
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        TouchBeginEvent?.Invoke(touch);
                        break;
                    case TouchPhase.Moved:
                    case TouchPhase.Stationary:
                        TouchProcessEvent?.Invoke(touch);
                        break;
                    case TouchPhase.Ended:
                    case TouchPhase.Canceled:
                        TouchEndedEvent?.Invoke(touch);
                        break;
                }
            }
            else if (UnityEngine.Input.touchCount >= 2)
            {
                int activeTouches = 0;
                int beginTouches = 0;

                for (int i = 0; i < UnityEngine.Input.touchCount; i++)
                {
                    var touch = UnityEngine.Input.GetTouch(i);
                    if (!(touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended || PointerExt.IsPointerOverUIElement(touch.position)))
                    {
                        if (activeTouches < 2) _touches[activeTouches] = touch;
                        activeTouches++;
                    }
                    if (touch.phase == TouchPhase.Began) beginTouches++;
                }
   
                if (activeTouches < 2)
                {
                    TwoFingersEndedEvent?.Invoke(_touches);
                    return;
                }

                if (activeTouches - beginTouches < 2)
                {
                    TwoFingersBeginEvent?.Invoke(_touches);
                    return;
                }

                TwoFingersTouchProcessEvent?.Invoke(_touches);
            }
        }

        private bool ValidMousePosition(out Vector2 mousePos)
        {
            var v = UnityEngine.Input.mousePosition;
            if ((v.x > 0 || v.y > 0 || v.x < Screen.width || v.y < Screen.height) && !PointerExt.IsPointerOverUIElement(v))
            {
                mousePos = v;
                return true;
            }
            mousePos = Vector2.zero;
            return false;
        }

    }
}
